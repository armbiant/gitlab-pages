package redirects

import (
	archZip "archive/zip"
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	netlifyRedirects "github.com/tj/go-redirects"

	"gitlab.com/gitlab-org/gitlab-pages/internal/confighelper"
	"gitlab.com/gitlab-org/gitlab-pages/internal/feature"
	"gitlab.com/gitlab-org/gitlab-pages/internal/testhelpers"
	"gitlab.com/gitlab-org/gitlab-pages/internal/vfs"
	"gitlab.com/gitlab-org/gitlab-pages/internal/vfs/zip"
)

func TestRedirectsRewrite(t *testing.T) {
	t.Setenv(feature.RedirectsPlaceholders.EnvVariable, "true")

	tests := []struct {
		name           string
		url            string
		rule           string
		expectedURL    string
		expectedStatus int
		expectedErr    error
	}{
		{
			name:           "No rules given",
			url:            "/no-redirect/",
			rule:           "",
			expectedURL:    "",
			expectedStatus: 0,
			expectedErr:    ErrNoRedirect,
		},
		{
			name:           "No matching rules",
			url:            "/no-redirect/",
			rule:           "/cake-portal.html  /still-alive.html 301",
			expectedURL:    "",
			expectedStatus: 0,
			expectedErr:    ErrNoRedirect,
		},
		{
			name:           "Matching rule redirects",
			url:            "/cake-portal.html",
			rule:           "/cake-portal.html  /still-alive.html 301",
			expectedURL:    "/still-alive.html",
			expectedStatus: http.StatusMovedPermanently,
		},
		{
			name:           "Does not redirect to invalid rule",
			url:            "/goto.html",
			rule:           "/goto.html GitLab.com 301",
			expectedURL:    "",
			expectedStatus: 0,
			expectedErr:    ErrNoRedirect,
		},
		{
			name:           "Matches trailing slash rule to no trailing slash URL",
			url:            "/cake-portal",
			rule:           "/cake-portal/  /still-alive/ 301",
			expectedURL:    "/still-alive/",
			expectedStatus: http.StatusMovedPermanently,
		},
		{
			name:           "Matches trailing slash rule to trailing slash URL",
			url:            "/cake-portal/",
			rule:           "/cake-portal/  /still-alive/ 301",
			expectedURL:    "/still-alive/",
			expectedStatus: http.StatusMovedPermanently,
		},
		{
			name:           "Matches no trailing slash rule to no trailing slash URL",
			url:            "/cake-portal",
			rule:           "/cake-portal  /still-alive 301",
			expectedURL:    "/still-alive",
			expectedStatus: http.StatusMovedPermanently,
		},
		{
			name:           "Matches no trailing slash rule to trailing slash URL",
			url:            "/cake-portal/",
			rule:           "/cake-portal  /still-alive 301",
			expectedURL:    "/still-alive",
			expectedStatus: http.StatusMovedPermanently,
		},
		{
			name:           "matches_splat_rule",
			url:            "/the-cake/is-delicious",
			rule:           "/the-cake/* /is-a-lie 200",
			expectedURL:    "/is-a-lie",
			expectedStatus: http.StatusOK,
		},
		{
			name:           "replaces_splat_placeholdes",
			url:            "/from/weighted/companion/cube/path",
			rule:           "/from/*/path /to/:splat/path 200",
			expectedURL:    "/to/weighted/companion/cube/path",
			expectedStatus: http.StatusOK,
		},
		{
			name:           "matches_placeholder_rule",
			url:            "/the/cake/is/delicious",
			rule:           "/the/:placeholder/is/delicious /the/:placeholder/is/a/lie 200",
			expectedURL:    "/the/cake/is/a/lie",
			expectedStatus: http.StatusOK,
		},
		{
			name:           "does_not_redirect_acme_challenges",
			url:            "/.well-known/acme-challenge/token",
			rule:           "/* /to/path 200",
			expectedURL:    "",
			expectedStatus: 0,
			expectedErr:    ErrNoRedirect,
		},
		{
			name:           "splat_rule_redirects_ending_with_number",
			url:            "/15.1",
			rule:           "/15.1/* /15/:splat 301",
			expectedURL:    "/15/",
			expectedStatus: http.StatusMovedPermanently,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := Redirects{}

			if tt.rule != "" {
				rules, err := netlifyRedirects.ParseString(tt.rule)
				require.NoError(t, err)
				r.rules = rules
			}

			url, err := url.Parse(tt.url)
			require.NoError(t, err)

			toURL, status, err := r.Rewrite(url)

			if tt.expectedURL != "" {
				require.Equal(t, tt.expectedURL, toURL.String())
			} else {
				require.Nil(t, toURL)
			}

			require.Equal(t, tt.expectedStatus, status)
			require.ErrorIs(t, err, tt.expectedErr)
		})
	}
}

func TestRedirectsParseRedirects(t *testing.T) {
	ctx := context.Background()

	tests := []struct {
		name          string
		redirectsFile string
		expectedRules int
		expectedErr   error
	}{
		{
			name:          "No `_redirects` file present",
			redirectsFile: "",
			expectedRules: 0,
			expectedErr:   errConfigNotFound,
		},
		{
			name:          "Everything working as expected",
			redirectsFile: `/goto.html /target.html 301`,
			expectedRules: 1,
		},
		{
			name:          "Invalid _redirects syntax gives no rules",
			redirectsFile: `foobar::baz`,
			expectedRules: 0,
		},
		{
			name:          "Config file too big",
			redirectsFile: strings.Repeat("a", 2*cfg.MaxConfigSize),
			expectedRules: 0,
			expectedErr:   errFileTooLarge,
		},
		// In future versions of `github.com/tj/go-redirects`,
		// this may not throw a parsing error and this test could be removed
		{
			name:          "Parsing error is caught",
			redirectsFile: "/store id=:id  /blog/:id  301",
			expectedRules: 0,
			expectedErr:   errFailedToParseConfig,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create a zip file with _redirects file populated based on redirectsCount
			zipContent, err := generateZipContent(tt.redirectsFile)
			require.NoError(t, err)

			// Serve the zip file with a mock HTTP server
			server := testhelpers.ServeZipFile(zipContent, "/public.zip")
			defer server.Close()

			defaultZipVFS := zip.New(&confighelper.DefaultZipCfgForTest)
			defer defaultZipVFS.Stop()

			// Create a VFS root from the served zip file
			vfsRoot, err := createVFSRoot(server.URL+"/public.zip", defaultZipVFS)
			require.NoError(t, err)

			redirects := ParseRedirects(ctx, vfsRoot)

			require.ErrorIs(t, redirects.error, tt.expectedErr)
			require.Len(t, redirects.rules, tt.expectedRules)
		})
	}
}

func TestMaxRuleCount(t *testing.T) {
	zipContent, err := generateZipContent(strings.Repeat("/goto.html /target.html 301\n", cfg.MaxRuleCount-1) +
		"/1000.html /target1000 301\n" +
		"/1001.html /target1001 301\n")
	require.NoError(t, err)

	// Serve the zip file with a mock HTTP server
	server := testhelpers.ServeZipFile(zipContent, "/public.zip")
	defer server.Close()

	defaultZipVFS := zip.New(&confighelper.DefaultZipCfgForTest)
	defer defaultZipVFS.Stop()

	// Create a VFS root from the served zip file
	vfsRoot, err := createVFSRoot(server.URL+"/public.zip", defaultZipVFS)
	require.NoError(t, err)

	redirects := ParseRedirects(context.Background(), vfsRoot)

	testFn := func(path, expectedToURL string, expectedStatus int, expectedErr error) func(t *testing.T) {
		return func(t *testing.T) {
			originalURL, err := url.Parse(path)
			require.NoError(t, err)

			toURL, status, err := redirects.Rewrite(originalURL)
			if expectedErr != nil {
				require.ErrorIs(t, err, expectedErr)
				return
			}

			require.NoError(t, err)

			require.Equal(t, expectedToURL, toURL.String())
			require.Equal(t, expectedStatus, status)
		}
	}

	t.Run("maxRuleCount matches", testFn("/1000.html", "/target1000", http.StatusMovedPermanently, nil))
	t.Run("maxRuleCount+1 does not match", testFn("/1001.html", "", 0, ErrNoRedirect))
}

// generateZipContent generates the content of a zip file with _redirects file populated based on redirectsCount
func generateZipContent(redirectContent string) ([]byte, error) {
	buff := new(bytes.Buffer)
	zw := archZip.NewWriter(buff)
	if len(redirectContent) > 0 {
		w, err := zw.Create(ConfigFile)
		if err != nil {
			return nil, err
		}
		_, err = io.WriteString(w, redirectContent)
		if err != nil {
			return nil, err
		}
	}
	err := zw.Close()
	if err != nil {
		return nil, err
	}
	return buff.Bytes(), nil
}

// createVFSRoot creates a VFS root from the served zip file
func createVFSRoot(url string, vfs vfs.VFS) (vfs.Root, error) {
	pathSha := testhelpers.Sha(url)
	return vfs.Root(context.Background(), url, pathSha)
}

// parseRedirectsFromVFS parses redirects from the VFS root
func parseRedirectsFromVFS(ctx context.Context, root vfs.Root) (*Redirects, error) {
	redirects := ParseRedirects(ctx, root)
	if redirects.error != nil {
		return nil, redirects.error
	}
	return redirects, nil
}
