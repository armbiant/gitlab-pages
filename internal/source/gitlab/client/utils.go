package client

import (
	"fmt"
	"net/url"
	"regexp"
	"strings"
)

// Reference for below limits: https://www.ietf.org/rfc/rfc1035.txt
const FQDNMaxLength = 255
const DNSLabelMaxLength = 63

var (
	ErrHostnameEmpty   = fmt.Errorf("hostname cannot be empty")
	ErrHostnameToLong  = fmt.Errorf("hostname is too long")
	ErrHostnameInvalid = fmt.Errorf("hostname is invalid")

	regexHostName = regexp.MustCompile(`^[a-zA-Z0-9._-]+$`)
)

// ValidateHostName Validates the hostname to ensure it is a valid and well-formed URL
func ValidateHostName(hostname string) error {
	if hostname == "" {
		return ErrHostnameEmpty
	}

	parsedURL, err := url.Parse(hostname)
	if err != nil {
		return err
	}
	path := parsedURL.Host
	if len(path) == 0 {
		// hostname does not contain url schema and hence on parsing, host is populated in path variable
		path = parsedURL.Path
	}

	// Check if the hostname is too long
	if len(path) > FQDNMaxLength {
		return ErrHostnameToLong
	}

	labels := strings.Split(path, ".")
	for _, label := range labels {
		if len(label) > DNSLabelMaxLength {
			return ErrHostnameToLong
		}
	}

	// Check if the hostname contains invalid characters
	if !isValidHostname(path) {
		return ErrHostnameInvalid
	}

	return nil
}

// isValidHostname checks if the hostname contains only valid characters (a-z, A-Z, 0-9, ., -, _)
func isValidHostname(hostname string) bool {
	return regexHostName.MatchString(hostname)
}
