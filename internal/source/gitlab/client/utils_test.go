package client

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestValidateHostName(t *testing.T) {
	tests := []struct {
		name      string
		hostname  string
		wantError error
	}{
		{
			name:      "Empty hostname",
			hostname:  "",
			wantError: fmt.Errorf("hostname cannot be empty"),
		},
		{
			name:      "Valid hostname",
			hostname:  "http://example.com",
			wantError: nil,
		},
		{
			name:      "Hostname too long",
			hostname:  "http://" + strings.Repeat("a", FQDNMaxLength+1) + ".com",
			wantError: ErrHostnameToLong,
		},
		{
			name:      "Label too long",
			hostname:  "http://" + strings.Repeat("a", DNSLabelMaxLength+1) + ".com",
			wantError: ErrHostnameToLong,
		},
		{
			name:      "Invalid characters in hostname",
			hostname:  "http://example!.com",
			wantError: ErrHostnameInvalid,
		},
		{
			name:      "Hostname with underscores",
			hostname:  "http://example_com",
			wantError: nil,
		},
		{
			name:      "Hostname with hyphens",
			hostname:  "http://example-com",
			wantError: nil,
		},
		{
			name:      "Hostname with mixed valid characters",
			hostname:  "http://example-123_com",
			wantError: nil,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			err := ValidateHostName(tt.hostname)
			if tt.wantError == nil {
				require.NoError(t, err)
			} else {
				require.EqualError(t, err, tt.wantError.Error())
			}
		})
	}
}
